
# varsub

## Introduction

- varsub is a simple bash script which prints a file (or files in a directory) where all '\$' prefixed **\$VARIABLES** are replaced with values in a **vars.env** file from the same directory.

- YAML files are printed with "**---**" (triple hyphen) appended (if "---" does not already exist on the final line of the file)

## Dependencies

- **bash** shell

## Installation

### Mac and Linux
Run the following command in a terminal with a POSIX compliant shell:

    git clone https://gitlab.com/bit-memo/devtools/varsub.git
    chmod +x varsub/varsub
    sudo mv varsub/varsub /usr/local/bin/varsub
    echo y | rm -R ./varsub

Since this is a private repo, your gitlab credentials are required

---
To uninstall, simply use:

    sudo rm /usr/local/bin/varsub

### Windows
This is not currently supported for windows (except through virtualising Linux, perhaps through cygwin etc)